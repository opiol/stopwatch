library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

entity TOP is
    port (
        mclk : in  std_logic;
        btn  : in  std_logic_vector(3 downto 0);
        led  : out std_logic_vector(7 downto 0);
        an   : out std_logic_vector(3 downto 0);
        ssg  : out std_logic_vector(7 downto 0)
        );
end TOP;

architecture Behavioral of TOP is

    signal rst             : std_logic;
    signal ce_100_HZ       : std_logic;
    signal btn_int_posedge : std_logic_vector(1 downto 0);

    signal start_stop : std_logic;
    signal lap_clear  : std_logic;
    signal tens    : std_logic_vector(3 downto 0);
    signal ones    : std_logic_vector(3 downto 0);
    signal tenths  : std_logic_vector(3 downto 0);
    signal hundths : std_logic_vector(3 downto 0);
    signal sseg_val: std_logic_vector(15 downto 0);
    
begin

    led(7 downto 3) <= "00000";
    rst             <= btn(3);
    led(2)          <= rst;
    
    start_stop <= btn_int_posedge(0);
    lap_clear  <= btn_int_posedge(1);
    
    sseg_val <= tens & ones & tenths & hundths ;

    puls_1 : entity work.puls
        generic map (
            div_factor => 500000)
        port map (
            clk   => mclk,
            pulse => ce_100_HZ);

    ssg(7) <= '1';                      -- decimal point off    
    seg_driver_1 : entity work.seg_driver
        port map (
            clk   => mclk,
            an    => an,
            ssg   => ssg(6 downto 0),
            value => sseg_val
            );

    btn_gen : for i in 0 to 1 generate
        btn_drv_x : entity work.btn_driver
            generic map (
                DEBOUNCE_CE_CLK => 4)
            port map (
                clk         => mclk,
                ce          => ce_100_HZ,
                rst         => rst,
                but_in      => btn(i),
                but_state   => open,
                but_posedge => btn_int_posedge(i),
                but_negedge => open);

        stretch_x : entity work.stretch
            generic map (
                STRETCH_CE_CLK => 10000000)
            port map (
                clk    => mclk,
                ce     => '1',
                rst    => rst,
                puls_i => btn_int_posedge(i),
                puls_o => led(i));
    end generate btn_gen;

    ---------------------------------------------------------------------------
    -- STOPWATCH FSM
    ---------------------------------------------------------------------------
    stopwatch_1: entity work.stopwatch
        port map (
            clk     => mclk,
            ce      => ce_100_HZ,
            rst     => rst,
            ss      => start_stop,
            lc      => lap_clear,
            tens    => tens,
            ones    => ones,
            tenths  => tenths,
            hundths => hundths);
    
end Behavioral;
