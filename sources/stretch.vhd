-------------------------------------------------------------------------------
-- Title      : stretch
-- Project    : 
-------------------------------------------------------------------------------
-- File       : stretch.vhd
-- Author     : opiol  <opiol@opiol-ThinkPad>
-- Company    : 
-- Created    : 2019-03-16
-- Last update: 2019-03-16
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: Pulse stretching with clock enable 
-------------------------------------------------------------------------------
-- Copyright (c) 2019 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2019-03-16  1.0      opiol   Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

-------------------------------------------------------------------------------

entity stretch is

    generic (
        STRETCH_CE_CLK : integer := 10
        );

    port (
        clk : in std_logic;
        ce  : in std_logic;
        rst : in std_logic;

        puls_i : in  std_logic;
        puls_o : out std_logic
        );

end entity stretch;

-------------------------------------------------------------------------------

architecture str of stretch is

    ---------------------------------------------------------------------------
    -- Internal signal declarations
    ---------------------------------------------------------------------------

    signal counter       : integer;
    signal output_active : std_logic;
begin  -- architecture str

    -- purpose: wait for set amount of CLKs
    -- type   : sequential
    -- inputs : clk, rst, ce, puls_i
    -- outputs: puls_o
    stretch_proc : process (clk) is
    begin  -- process stretch_proc
        if rising_edge(clk) then           -- rising clock edge
            if rst = '1' then              -- synchronous reset (active high)
                counter <= 0;
                puls_o  <= '0';
            else
                if ce = '1' then
                    output_active <= '0';  -- default              
                    if puls_i = '1' then
                        output_active <= '1';
                    end if;


                    if output_active = '1' then
                        if counter /= STRETCH_CE_CLK then
                            counter       <= counter +1;
                            puls_o        <= '1';
                            output_active <= '1';
                        else
                            output_active <= '0';
                            counter       <= 0;
                            puls_o        <= '0';
                        end if;
                    end if;
                end if;
            end if;
        end if;
    end process stretch_proc;

end architecture str;

-------------------------------------------------------------------------------
