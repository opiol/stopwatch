-------------------------------------------------------------------------------
-- Title      : stopwatch
-- Project    : 
-------------------------------------------------------------------------------
-- File       : stopwatch.vhd
-- Author     : opiol  <opiol@opiol-ThinkPad>
-- Company    : 
-- Created    : 2019-03-17
-- Last update: 2019-03-17
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: Internal stopwatch structure - expects 1 
-------------------------------------------------------------------------------
-- Copyright (c) 2019 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2019-03-17  1.0      opiol	Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

-------------------------------------------------------------------------------

entity stopwatch is
    port (
        clk     : in std_logic;
        ce      : in std_logic;
        rst     : in std_logic;

        ss      : in std_logic;
        lc      : in std_logic;

        tens    :out std_logic_vector(3 downto 0);
        ones    :out std_logic_vector(3 downto 0);
        tenths  :out std_logic_vector(3 downto 0);
        hundths :out std_logic_vector(3 downto 0)
        );

end entity stopwatch;

-------------------------------------------------------------------------------

architecture str of stopwatch is

    ---------------------------------------------------------------------------
    -- Internal signal declarations
    ---------------------------------------------------------------------------

    signal cnt_ena : std_logic;
    signal cnt_rst : std_logic;
    signal disp_en : std_logic;

    signal in_a     : std_logic_vector(3 downto 0);
    signal in_b     : std_logic_vector(3 downto 0);
    signal in_c     : std_logic_vector(3 downto 0);
    signal in_d     : std_logic_vector(3 downto 0);


begin  -- architecture str

    bcd_cnt_1: entity work.bcd_cnt
        port map (
            clk       => clk,
            ce_100_hz => ce,
            srst      => cnt_rst,
            cnt_ena   => cnt_ena,
            cnt_3     => in_a,
            cnt_2     => in_b,
            cnt_1     => in_c,
            cnt_0     => in_d);

    stopwatch_fsm_1: entity work.stopwatch_fsm
        port map (
            clk       => clk,
            ss        => ss,
            lc        => lc,
            cnt_ena_o => cnt_ena,
            cnt_rst_o => cnt_rst,
            disp_en_o => disp_en);

    display_reg_1: entity work.display_reg
        port map (
            clk      => clk,
            ce       => '1',
            disp_ena => disp_en,
            in_a     => in_a,
            in_b     => in_b,
            in_c     => in_c,
            in_d     => in_d,
            out_a    => tens,
            out_b    => ones,
            out_c    => tenths,
            out_d    => hundths);
end architecture str;

-------------------------------------------------------------------------------
