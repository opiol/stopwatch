library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;



entity bcd_cnt is
    port (clk       : in std_logic;
          ce_100_hz : in std_logic;
          srst      : in std_logic;

          cnt_ena : in  std_logic;
          cnt_3   : out std_logic_vector(3 downto 0);
          cnt_2   : out std_logic_vector(3 downto 0);
          cnt_1   : out std_logic_vector(3 downto 0);
          cnt_0   : out std_logic_vector(3 downto 0)
          );
end bcd_cnt;

architecture Behavioral of bcd_cnt is

    signal val_3 : unsigned (3 downto 0) := X"0";
    signal val_2 : unsigned (3 downto 0) := X"0";
    signal val_1 : unsigned (3 downto 0) := X"0";
    signal val_0 : unsigned (3 downto 0) := X"0";

begin
    proc : process(clk)
    begin
        if rising_edge(clk) then
            if srst = '1' then
                val_0 <= X"0";
                val_1 <= X"0";
                val_2 <= X"0";
                val_3 <= X"0";
            else

                if ce_100_hz = '1' and cnt_ena = '1' then

                    if val_0 = "1001" and val_1 = "1001" and val_2 = "1001" and val_3 = "0101" then
                        val_0 <= X"0";
                        val_1 <= X"0";
                        val_2 <= X"0";
                        val_3 <= X"0";

                    elsif val_0 = "1001" and val_1 = "1001" and val_2 = "1001" then
                        val_0 <= X"0";
                        val_1 <= X"0";
                        val_2 <= X"0";

                        val_3 <= val_3 + 1;

                    elsif val_0 = "1001" and val_1 = "1001" then
                        val_0 <= X"0";
                        val_1 <= X"0";

                        val_2 <= val_2 + 1;

                    elsif val_0 = "1001" then
                        val_0 <= X"0";

                        val_1 <= val_1 + 1;
                    else
                        val_0 <= val_0 + 1;
                    end if;
                end if;
            end if;
            cnt_0 <= std_logic_vector(val_0);
            cnt_1 <= std_logic_vector(val_1);
            cnt_2 <= std_logic_vector(val_2);
            cnt_3 <= std_logic_vector(val_3);
        end if;
    end process proc;

end Behavioral;
