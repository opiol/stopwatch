library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity display_reg is
    port(
        clk : in std_logic;
        ce  : in std_logic;

        disp_ena : in std_logic;

        in_a : in std_logic_vector(3 downto 0);
        in_b : in std_logic_vector(3 downto 0);
        in_c : in std_logic_vector(3 downto 0);
        in_d : in std_logic_vector(3 downto 0);

        out_a : out std_logic_vector(3 downto 0);
        out_b : out std_logic_vector(3 downto 0);
        out_c : out std_logic_vector(3 downto 0);
        out_d : out std_logic_vector(3 downto 0)

        );
end display_reg;

architecture arch of display_reg is

    signal int_reg_a : std_logic_vector(3 downto 0);
    signal int_reg_b : std_logic_vector(3 downto 0);
    signal int_reg_c : std_logic_vector(3 downto 0);
    signal int_reg_d : std_logic_vector(3 downto 0);

begin
    process(clk)
    begin
        if rising_edge(clk) then
            if ce = '1' then
                out_a <= int_reg_a;
                out_b <= int_reg_b;
                out_c <= int_reg_c;
                out_d <= int_reg_d;

                if disp_ena = '1' then
                    int_reg_a <= in_a;
                    int_reg_b <= in_b;
                    int_reg_c <= in_c;
                    int_reg_d <= in_d;
                end if;

            end if;
        end if;
    end process;

end architecture;
