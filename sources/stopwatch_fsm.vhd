library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity stopwatch_fsm is
    port(
        clk : in std_logic;

        ss : in std_logic;
        lc : in std_logic;

        cnt_ena_o : out std_logic;
        cnt_rst_o : out std_logic;
        disp_en_o : out std_logic
        );
end stopwatch_fsm;

architecture arch of stopwatch_fsm is

    signal cnt_ena : std_logic;
    signal cnt_rst : std_logic;
    signal disp_en : std_logic;

    type t_state is (st_idle, st_run, st_lap, st_refresh, st_stop);

    signal state      : t_state := st_idle;
    signal next_state : t_state;

begin

    sync_proc : process(clk)
    begin
        if rising_edge(clk) then
            state <= next_state;
        end if;
    end process sync_proc;


    next_state_decode : process(state, ss, lc)
    begin
        next_state <= state;
        case state is
            when st_idle =>
                if ss = '1' then
                    next_state <= st_run;
                end if;

            when st_run =>
                if ss = '1' then
                    next_state <= st_stop;
                end if;
                if lc = '1' then
                    next_state <= st_lap;
                end if;

            when st_lap =>
                if ss = '1' then
                    next_state <= st_run;
                end if;
                if lc = '1' then
                    next_state <= st_refresh;
                end if;

            when st_refresh =>
                next_state <= st_lap;

            when st_stop =>
                if ss = '1' then
                    next_state <= st_run;
                end if;
                if lc = '1' then
                    next_state <= st_idle;
                end if;
        end case;
    end process next_state_decode;

    out_decode_proc : process(state)
    begin
        case state is
            when st_idle =>
                disp_en <= '1';
                cnt_ena <= '0';
                cnt_rst <= '1';

            when st_run =>
                disp_en <= '1';
                cnt_ena <= '1';
                cnt_rst <= '0';

            when st_lap =>
                disp_en <= '0';
                cnt_ena <= '1';
                cnt_rst <= '0';

            when st_refresh =>
                disp_en <= '1';
                cnt_ena <= '1';
                cnt_rst <= '0';

            when st_stop =>
                disp_en <= '1';
                cnt_ena <= '0';
                cnt_rst <= '0';
        end case;
    end process out_decode_proc;

    out_sync : process(clk)
    begin
        if rising_edge(clk) then
            cnt_ena_o <= cnt_ena;
            cnt_rst_o <= cnt_rst;
            disp_en_o <= disp_en;
        end if;
    end process out_sync;
end architecture;
