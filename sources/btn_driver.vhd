-------------------------------------------------------------------------------
-- Title      : btn_driver
-- Project    : 
-------------------------------------------------------------------------------
-- File       : btn_driver.vhd
-- Author     : opiol  <opiol@opiol-ThinkPad>
-- Company    : 
-- Created    : 2019-03-16
-- Last update: 2019-03-17
-- Platform   : 
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: Metastability remover, Deboucer, edge detector
-------------------------------------------------------------------------------
-- Copyright (c) 2019 
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2019-03-16  1.0      opiol   Created
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

-------------------------------------------------------------------------------

entity btn_driver is

    generic (
        DEBOUNCE_CE_CLK : integer := 16
        );

    port (
        clk : in std_logic;
        ce  : in std_logic;
        rst : in std_logic;

        but_in      : in  std_logic;
        but_state   : out std_logic;
        but_posedge : out std_logic;
        but_negedge : out std_logic
        );

end entity btn_driver;

-------------------------------------------------------------------------------

architecture str of btn_driver is

    ---------------------------------------------------------------------------
    -- Internal signal declarations
    ---------------------------------------------------------------------------

    signal input_metastab_1                     : std_logic;
    signal input_metastab_2                     : std_logic;
    attribute SHREG_EXTRACT                     : string;
    attribute SHREG_EXTRACT of input_metastab_1 : signal is "NO";
    attribute SHREG_EXTRACT of input_metastab_2 : signal is "NO";

    signal but_state_int : std_logic;
    signal deb_reg       : std_logic_vector(DEBOUNCE_CE_CLK -1 downto 0);
    constant ones        : std_logic_vector(DEBOUNCE_CE_CLK -1 downto 0) := (others => '1');
    constant zeros       : std_logic_vector(DEBOUNCE_CE_CLK -1 downto 0) := (others => '0');

    signal edge_vec : std_logic_vector(1 downto 0);

begin  -- architecture str

    -- purpose: remove metastability from input bu double D's
    -- type   : sequential
    -- inputs : clk, rst ,but_in
    -- outputs: input_debounce_2
    metastab_proc : process (clk) is
    begin  -- process metastab_proc
        if rising_edge(clk) then        -- rising clock edge
            if rst = '1' then           -- synchronous reset (active high)
                input_metastab_1 <= '0';
                input_metastab_2 <= '0';
            else
                input_metastab_1 <= but_in;
                input_metastab_2 <= input_metastab_1;
            end if;
        end if;
    end process metastab_proc;

    -- purpose: debouncing proces
    -- type   : sequential
    -- inputs : clk, rst, input_metastab_2, DEBOUNCE_CE_CLK
    -- outputs: but_state_int
    debouce_proc : process (clk) is
    begin  -- process debouce_proc
        if rising_edge(clk) then        -- rising clock edge
            if rst = '1' then           -- synchronous reset (active high)
                but_state_int <= '0';
                deb_reg       <= (others => '0');
            else
                if ce = '1' then
                    deb_reg <= deb_reg(deb_reg'high -1 downto 0) & input_metastab_2;
                    if deb_reg = ones then
                        but_state_int <= '1';
                    elsif deb_reg = zeros then
                        but_state_int <= '0';
                    end if;
                end if;
            end if;
        end if;
    end process debouce_proc;

    but_state <= but_state_int;

    -- purpose: detect rising/falling edge from debounced signal
    -- type   : sequential
    -- inputs : clk, rst, ce, but_state_int
    -- outputs: posedge, negedge - only 1 fast clk
    edge_proc : process (clk) is
    begin  -- process edge_proc
        if rising_edge(clk) then        -- rising clock edge
            if rst = '1' then           -- synchronous reset (active high)
                but_posedge <= '0';
                but_negedge <= '0';
                edge_vec    <= "00";
            else
                but_negedge <= '0';
                but_posedge <= '0';
                if ce = '1' then
                    edge_vec(0) <= but_state_int;
                    edge_vec(1) <= edge_vec(0);

                    if edge_vec(1) /= edge_vec(0) then
                        if edge_vec(1) = '0' then
                            but_posedge <= '1';
                        else
                            but_negedge <= '1';
                        end if;
                    end if;
                end if;
            end if;
        end if;
    end process edge_proc;

end architecture str;

-------------------------------------------------------------------------------
