library ieee;
use ieee.STD_LOGIC_1164.all;
use ieee.STD_LOGIC_ARITH.all;
use ieee.std_logic_unsigned.all;

entity seg_driver is
    port(
        clk   : in  std_logic;
        an    : out std_logic_vector(3 downto 0);
        ssg   : out std_logic_vector(6 downto 0);
        value : in  std_logic_vector(15 downto 0)
        );
end seg_driver;

architecture arch of seg_driver is

    signal an_current : unsigned(1 downto 0) := "00";
    signal val        : std_logic_vector(3 downto 0);
    signal pulse      : std_logic;

begin

    puls_1 : entity work.puls
        generic map (
            div_factor => 50000)
        port map (
            clk   => clk,
            pulse => pulse);

    disp_proc : process (clk) is
    begin
        if rising_edge(clk) then
            if pulse = '1' then
                an_current <= an_current + 1;

                case an_current is
                    when "00" =>
                        an  <= "0111";
                        val <= value(3 downto 0);
                    when "01" =>
                        an  <= "1011";
                        val <= value(7 downto 4);
                    when "10" =>
                        an  <= "1101";
                        val <= value(11 downto 8);
                    when "11" =>
                        an  <= "1110";
                        val <= value(15 downto 12);
                    when others => null;
                end case;
            end if;
        end if;
    end process disp_proc;


    out_proc : process(val) is
    begin
        case val is
            when X"0"   => ssg <= "0000001";
            when X"1"   => ssg <= "1001111";
            when X"2"   => ssg <= "0010010";
            when X"3"   => ssg <= "0000110";
            when X"4"   => ssg <= "1001100";
            when X"5"   => ssg <= "0100100";
            when X"6"   => ssg <= "0100000";
            when X"7"   => ssg <= "0001111";
            when X"8"   => ssg <= "0000000";
            when X"9"   => ssg <= "0000100";
            when X"A"   => ssg <= "0001000";
            when X"B"   => ssg <= "1100000";
            when X"C"   => ssg <= "0110001";
            when X"D"   => ssg <= "1000010";
            when X"E"   => ssg <= "0110000";
            when X"F"   => ssg <= "0111000";
            when others => null;
        end case;

    end process out_proc;
end arch;




